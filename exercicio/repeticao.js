//comentario de 1 linha
/*
bloco de comentario
*/
let numero = 0;
while(numero <=10){
    console.log(`valor nr: ${(numero)}`);
    if(numero%2==0){
        console.log(`valor nr: ${(numero)} é PAR!`);
    }else{
        console.log(`valor nr: ${(numero)} é ÍMPAR`);

    }
    numero++;
}
console.log("REPETIÇÃO DO/WHILE");
let numero1 = 0;
do{
    console.log(`valor nr: ${(numero1)}`);
    numero1++;
}while(numero1<=10);

console.log("REPETIÇÃO FOR");
for(let numero2=0; numero2<=10; numero2++){
    console.log(`Valor nr: ${(numero2)}`);
}